## FIRMWARE DUMP
### sys_tssi_64_armv82_infinix-user 14 UP1A.231005.007 697388 release-keys
- Transsion Name: Infinix ZERO 30 5G
- TranOS Build: X6731-H935AB-U-RU-240909V481
- TranOS Version: xos14.5.0
- Brand: INFINIX
- Model: Infinix-X6731
- Platform: mt6893 (Dimensity 8020)
- Android Build: UP1A.231005.007
- Android Version: 14
- Kernel Version: 5.10.209
- Security Patch: 2024-08-05
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Screen Density: 480
